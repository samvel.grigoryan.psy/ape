import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import {TranslateService} from "@ngx-translate/core";
import {LanguageService} from "../../services/language/language.service";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  isLoggedIn = false;

  constructor(private auth: AuthService,private translate: LanguageService) {
  }

  ngOnInit(): void {
    this.isLoggedIn = this.auth.isLoggedIn();
  }

  changeLanguage(language: string) {
    this.translate.setLanguage(language);
  }
}
