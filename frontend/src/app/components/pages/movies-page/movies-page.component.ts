import {Component, OnInit} from '@angular/core';
import {Observable, tap} from "rxjs";
import {IMovie} from "../../../models/movie";
import {MovieService} from "../../../services/movie/movie.service";
import {LoaderService} from "../../../services/loader/loader.service";

@Component({
  selector: 'app-movies-page',
  templateUrl: './movies-page.component.html',
  styleUrls: ['./movies-page.component.scss']
})

export class MoviesPageComponent implements OnInit {
  dailyMovie$: Observable<IMovie>
  movieId = "tt0000002"

  constructor(private _movieService: MovieService, public loaderService: LoaderService) {
  }

  ngOnInit(): void {
    this.loaderService.show();
    this.dailyMovie$ = this._movieService.getMovieById(this.movieId).pipe(
      tap(() => {
        this.loaderService.hide();
      })
    );
  }
}
