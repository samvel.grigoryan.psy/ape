import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router, ParamMap} from '@angular/router';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-product-page',
  templateUrl: './product-detail-page.component.html',
  styleUrls: ['./product-detail-page.component.scss'],
})
export class ProductDetailPageComponent implements OnInit {
  public productId: string | null;
  private routeParamSubscription: Subscription;
  constructor(private activatedRoute: ActivatedRoute, private route: Router) {
  }

  ngOnInit(): void {
    this.routeParamSubscription =  this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.productId = params.get('id');
    })
  }

  ngOnDestroy(): void {
    if (this.routeParamSubscription) {
      this.routeParamSubscription.unsubscribe();
    }
  }

  navigatePrevious() {
    let previousId: number = Number(this.productId);
    previousId--;
    this.route.navigate(['/product', previousId]);
  }

  navigateNext() {
    let nextId: number = Number(this.productId);
    nextId++;
    this.route.navigate(['/product', nextId]);
  }
}
