import {Component, OnInit} from '@angular/core';
import {Observable, tap} from "rxjs";
import {IProduct} from "../../../models/product";
import {ProductsService} from "../../../services/products/products";
import {LoaderService} from "../../../services/loader/loader.service";

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss'],
  providers: [ProductsService]
})
export class ProductPageComponent implements OnInit {
  name = '';
  price = 10000;
  // products: IProduct[] = [];
  products$: Observable<IProduct[]>

  constructor(private productsService: ProductsService, public loaderService: LoaderService) {
  }
  ngOnInit(): void {
    this.loaderService.show();

    this.products$ = this.productsService.getAll().pipe(
      tap(() => {
        this.loaderService.hide();
      })
    );
  }

}
