import { Component, Input, OnInit } from '@angular/core';
import { LoaderService } from '../../../services/loader/loader.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { catchError, Observable, Subscription, tap, throwError } from 'rxjs';
import { IMovie } from '../../../models/movie';
import { MovieService } from '../../../services/movie/movie.service';

@Component({
  selector: 'app-movie-page',
  templateUrl: './movie-page.component.html',
  styleUrls: ['./movie-page.component.scss']
})

export class MoviePageComponent implements OnInit {
  public movieId: string | null;
  public isFavorite: boolean | null;
  private routeParamSubscription: Subscription;
  movie$: Observable<IMovie>;

  constructor(private _movieService: MovieService, private activatedRoute: ActivatedRoute, public loaderService: LoaderService) {
  }

  ngOnInit(): void {
    this.routeParamSubscription = this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.loaderService.show();
      this.getParamId(params);
      this.getMovie();
    });
  }

  getParamId(params: ParamMap) {
    this.movieId = params.get('id');
    if (!this.movieId || this.movieId.length === 0) {
      catchError(this.errorHandler.bind(this));
      return;
    }
  }

  getMovie() {
    this.movie$ = this._movieService.getMovieById(this.movieId!).pipe(
      tap(() => {
        this.loaderService.hide();
      })
    );
  }

  getIsFavorite(): boolean {
    const storedValue = localStorage.getItem(this.movieId!);
    if (storedValue) {
      return JSON.parse(storedValue).isFavorite;
    } else {
      return false;
    }
  }

  setMovieFavorite() {
    let objectValue = { isFavorite: true };

    if (this.getIsFavorite()) {
      objectValue = { isFavorite: false };
    }
    localStorage.setItem(this.movieId!, JSON.stringify(objectValue));
  }

  private errorHandler() {
    return throwError(() => 'Invalid Id');
  }
}
