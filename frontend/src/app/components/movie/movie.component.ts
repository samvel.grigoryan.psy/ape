import {Component, Input} from "@angular/core";
import {Router} from "@angular/router";
import {IMovieItem} from "../../models/movie";

@Component({
  styleUrls: ['./movie.component.scss'],
  selector: 'app-movie',
  templateUrl: './movie.component.html'
})

export class MovieComponent {
  @Input() movie: IMovieItem

  constructor(private router: Router) {
  }

  openDetail(movie: IMovieItem) {

    this.router.navigate(['movieId/', movie.id]);
  }
}
