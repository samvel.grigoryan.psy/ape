import {Component, OnInit} from '@angular/core';
import {MovieService} from "../../services/movie/movie.service";
import {Observable, tap} from "rxjs";
import {IMovieList} from "../../models/movie";
import {LoaderService} from "../../services/loader/loader.service";
import {FilterMoviesPipe} from "../../pipes/filter-movies.pipe";

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
  providers: [MovieService,FilterMoviesPipe]
})

export class MovieListComponent implements OnInit {
  searchResultMovie$: Observable<IMovieList>
  searchResultMovies: IMovieList;
  name = 'c';
  isSearchInactive = false;
  filterItems = ['alphabetical', 'oldest']
  selectedDropdownItem = 'alphabetical';

  constructor(private _movieService: MovieService, public loaderService: LoaderService, private movieFilterPipe: FilterMoviesPipe) {
  }

  ngOnInit(): void {
    this.updateMovies();
  }

  onItemSelected(item: any) {
    this.searchResultMovie$ = this.movieFilterPipe.transform(this.searchResultMovie$,item);
  }

  updateMovies() {
    this.loaderService.show();
    this.isSearchInactive = true;
    this.searchResultMovie$ = this._movieService.searchMovie(this.name, true).pipe(
      tap((data) => {
        this.isSearchInactive = false;
        this.loaderService.hide();
      })
    )
  }
}
