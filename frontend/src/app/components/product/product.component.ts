import {Component, Input} from "@angular/core";
import {IProduct} from "../../models/product";
import {Router} from "@angular/router";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html'
})

export class ProductComponent {

  details = false;
  @Input() product: IProduct

  constructor(private router: Router) {
  }

  openDetail(product: IProduct) {
    this.router.navigate(['product/', product.id]);
  }
}
