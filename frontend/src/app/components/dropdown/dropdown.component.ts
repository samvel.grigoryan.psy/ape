import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent {
  @Input() items: string[];
  @Input() selectedItem: string | null;
  @Output() itemSelected = new EventEmitter<any>();

  isActive: boolean = false;

  selectItem(item: string) {
    this.selectedItem = item;
    this.itemSelected.emit(item);
    this.isActive = false;
  }

}
