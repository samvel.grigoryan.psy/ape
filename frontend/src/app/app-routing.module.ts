import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MoviesPageComponent } from './components/pages/movies-page/movies-page.component';
import { PageNotFoundComponent } from './components/pages/page-not-found/page-not-found.component';
import { ProductDetailPageComponent } from './components/pages/product-detail-page/product-detail-page.component';
import { MoviePageComponent } from './components/pages/movie-page/movie-page.component';
import { LoginComponent } from './components/login/login.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import {  AuthGuard } from './guards/auth.guard';
import { ProductPageComponent } from './components/pages/product-page/product-page.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'movies', component: MoviesPageComponent },
  { path: 'movieId/:id', component: MoviePageComponent },
  { path: 'products', component: ProductPageComponent },
  { path: 'product/:id', component: ProductDetailPageComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'admin',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/admin/admin.module')
        .then((m) => m.AdminModule)
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
