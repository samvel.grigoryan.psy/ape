import {Component} from '@angular/core';
import {LanguageService} from "./services/language/language.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  param = {value: 'gago'};

  constructor(languageService: LanguageService) {
    languageService.initLanguage()
  }
}


