import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ProductComponent} from "./components/product/product.component";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {ErrorComponent} from './components/error/error.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FilterProductsPipe, FilterProductsPricePipe} from './pipes/filter-products.pipe';
import {ModalComponent} from './components/modal/modal.component';
import {ProductPageComponent} from './components/pages/product-page/product-page.component';
import {MoviesPageComponent} from './components/pages/movies-page/movies-page.component';
import {NavigationComponent} from './components/navigation/navigation.component';
import {MovieListComponent} from './components/movie-list/movie-list.component';
import {ProductDetailPageComponent} from "./components/pages/product-detail-page/product-detail-page.component";
import {MovieComponent} from "./components/movie/movie.component";
import {LoaderComponent} from "./components/loader/loader.component";
import {DropdownComponent} from './components/dropdown/dropdown.component';
import {FilterMoviesPipe} from "./pipes/filter-movies.pipe";
import {MatIconModule} from '@angular/material/icon';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MoviePageComponent} from './components/pages/movie-page/movie-page.component';
import {LoginComponent} from './components/login/login.component';
import {ForgotPasswordComponent} from './components/forgot-password/forgot-password.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {LanguageService} from "./services/language/language.service";

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    ErrorComponent,
    LoaderComponent,
    FilterProductsPipe,
    FilterMoviesPipe,
    ModalComponent,
    ProductPageComponent,
    MoviesPageComponent,
    MoviePageComponent,
    NavigationComponent,
    FilterProductsPricePipe,
    MovieListComponent,
    ProductDetailPageComponent,
    MovieListComponent,
    MovieComponent,
    DropdownComponent,
    LoginComponent,
    ForgotPasswordComponent
  ],
  imports: [
    BrowserModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NoopAnimationsModule,
    MatIconModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  exports: [
    TranslateModule
  ],
  providers: [LanguageService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
