import { ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth/auth.service';
import { inject, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root', // or provide it in a specific module if needed
})
export  class AuthGuard {
  constructor(private auth: AuthService, private route: Router) {
  }
  canActivate(): boolean {
    if(!this.auth.isLoggedIn()){
      this.route.navigate(['login']);
      return false;
    }
    return this.auth.isLoggedIn()
  }
}
