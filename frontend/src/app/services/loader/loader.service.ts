import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class LoaderService {
  private loaderSubject = new BehaviorSubject<boolean>(false);
  loaderState = this.loaderSubject.asObservable();

  hide(){
    this.loaderSubject.next(false);
  }
  show(){
    this.loaderSubject.next(true);
  }
}
