import {Injectable} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";

@Injectable({
  providedIn: 'root',
})

export class LanguageService {
  private languageKey = 'lang';
  public languages = ['en', 'fr'];
  private defaultLanguage = this.languages[0];

  constructor(private translate: TranslateService) {

  }

  public initLanguage() {
    if (!this.getLanguage()) {
      this.setLanguage(this.defaultLanguage);
    } else {
      this.translate.use(this.getLanguage()!);
    }
  }

  public getLanguage() {
    return localStorage.getItem(this.languageKey);
  }

  public setLanguage(language: string) {
    localStorage.setItem(this.languageKey, language);
    this.translate.use(language);
  }
}
