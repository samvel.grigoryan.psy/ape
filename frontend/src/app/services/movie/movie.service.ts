import {Injectable} from '@angular/core';
import {catchError, Observable, retry, throwError} from "rxjs";
import {HttpClient, HttpErrorResponse, HttpParams} from "@angular/common/http";
import {IMovie, IMovieList} from "../../models/movie";
import {ErrorService} from "../error/error.service";

@Injectable({
  providedIn: 'root'
})

export class MovieService {

  constructor(
    private http: HttpClient,
    private errorService: ErrorService) {
  }

  private API_URL = 'https://moviesdatabase.p.rapidapi.com/'

  headers = {
    'X-RapidAPI-Key': '23598f74b5msh14acb00a4f2ad17p170902jsn485a576616f5',
    'X-RapidAPI-Host': 'moviesdatabase.p.rapidapi.com'
  };

  getMovieById(id:string): Observable<IMovie> {
    return this.http.get<IMovie>(this.API_URL + `titles/${id}`,
      {
        headers: this.headers
      }).pipe(
      catchError(this.errorHandler.bind(this))
    );
  }

  searchMovie(key:string, isTitle:boolean): Observable<IMovieList> {
    const searchApi = this.API_URL  + (isTitle? 'titles/search/title/' : 'titles/search/keyword/') + key ;
    return this.http.get<IMovieList>(searchApi,
      {
        headers: this.headers
      }).pipe(
      catchError(this.errorHandler.bind(this))
    );
  }

  private errorHandler(error: HttpErrorResponse) {
    // this.errorService.handle(error.message)
    return throwError(() => error.message)
  }
}

