import { Pipe, PipeTransform } from '@angular/core';
import {IProduct} from "../models/product";

@Pipe({
  name: 'filterProducts'
})
export class FilterProductsPipe implements PipeTransform {

  transform(products: IProduct[],search: string): IProduct[] {
    return products.filter(p => p.title.toLowerCase().includes(search.toLowerCase()));
  }
}
@Pipe({
  name: 'filterProductsPrice'
})
export class FilterProductsPricePipe implements PipeTransform {

  transform(products: IProduct[],price: number): IProduct[] {
    return products.filter(p => p.price < price);
  }

}
