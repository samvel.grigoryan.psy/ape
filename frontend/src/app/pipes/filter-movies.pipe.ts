import {Pipe, PipeTransform} from '@angular/core';
import {IMovieItem, IMovieList} from "../models/movie";
import {map, Observable} from "rxjs";

@Pipe({
  name: 'filterMovies'
})
export class FilterMoviesPipe implements PipeTransform {

  transform(movies: Observable<IMovieList>, filterType: string): Observable<IMovieList> {
    return movies.pipe(
      map(movies => {

        if (filterType === 'oldest') {
          movies.results.sort((a, b) => {
            const releaseDateA = a.releaseDate.year;
            const releaseDateB = b.releaseDate.year;
            return releaseDateA - releaseDateB;
          });
        } else if (filterType === 'alphabetical') {
          movies.results.sort((a, b) => {
            const nameA = a.titleText.text.toLowerCase();
            const nameB = b.titleText.text.toLowerCase();
            return nameA.localeCompare(nameB);
          });
        }
        // Handle other filter types or invalid filterType values here
        return movies;
      })
    );
  }
}
