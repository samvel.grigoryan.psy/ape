interface Image {
  id: string;
  width: number;
  height: number;
  url: string;
  caption: {
    plainText: string;
    __typename: string;
  };
  __typename: string;
}

interface TitleType {
  text: string;
  id: string;
  isSeries: boolean;
  isEpisode: boolean;
  __typename: string;
}

interface TitleText {
  text: string;
  __typename: string;
}

interface YearRange {
  year: number;
  endYear: number | null;
  __typename: string;
}

interface ReleaseDate {
  day: number;
  month: number;
  year: number;
  __typename: string;
}

export interface IMovie {
  results: IMovieItem;
}
export interface IMovieList {
  page: number;
  next: string;
  entries: number;
  results: IMovieItem[];
}
export interface IMovieItem {
  _id: string;
  id: string;
  primaryImage: Image;
  titleType: TitleType;
  titleText: TitleText;
  originalTitleText: TitleText;
  releaseYear: YearRange;
  releaseDate: ReleaseDate;
}


